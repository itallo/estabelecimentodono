<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8" />

	<title>meusalaoweb</title>
	<meta name="author" content="Dose - dose.ag" />
	 <meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Google Chrome Frame for IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="google-site-verification" content="" />
    
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="build/materialize/css/materialize.css">
	<link rel="stylesheet" href="build/css/style.css" />
	<script src="build/materialize/js/materialize.js"></script>
	<!--<script src="build/materialize/js/script.js"></script>-->
	<script src="build/materialize/js/complementoscript.js"></script>
	
	<script src="build/materialize/js/js/init.js"></script>
<!--Complemento Script-->
	<script src="build/materialize/js/js/date_picker/picker.date.js"></script>
	<script src="build/materialize/js/js/date_picker/picker.js"></script>
	<script src="build/materialize/js/js/animation.js"></script>
	<script src="build/materialize/js/js/buttons.js"></script>
	<script src="build/materialize/js/js/cards.js"></script>
	<script src="build/materialize/js/js/carousel.js"></script>
	<script src="build/materialize/js/js/character_counter.js"></script>
	<script src="build/materialize/js/js/chips.js"></script>
	<script src="build/materialize/js/js/collapsible.js"></script>
	<script src="build/materialize/js/js/dropdown.js"></script>
	<script src="build/materialize/js/js/forms.js"></script>
	<script src="build/materialize/js/js/global.js"></script>
	<script src="build/materialize/js/js/hammer.min.js"></script>

</head>
<body class="form">
   <div class="container">
     <div class="col-md-10 col-md-offset-1">
      <div class="caixa">
        <h2>Cadastro de Estabelecimento</h2>
         <div class="formulario">
         <div class="col-md-4 col-md-offset-1">
             <div class="row"><input type="text" placeholder="Nome :"></div>
          <div class="row"><input type="text" placeholder="E-mail :"></div>
          <div class="row"><input type="text" placeholder="DDD + Contato :"></div>
             <div class="row"><input type="text" placeholder="Estado :"></div>

                  </div>
         <div class="col-md-4 col-md-offset-1">
          <div class="row"><input type="text" placeholder="Cidade :"></div>
          <div class="row"><input type="text" placeholder="Bairro :"></div>
          <div class="row"><input type="text" placeholder="Rua :"></div>
          <div class="col-md-5"><input type="text" placeholder="Número :"></div>
          <div class="col-md-6 col-md-offset-1"><input type="text" placeholder="Complemento :"></div>
         </div>
          
                    
      </div>
      <button class="btn waves-effect waves-light" type="submit" name="action">Cadastrar
    <i class="material-icons right">send</i>
  </button> 
      </div>
     </div>
   </div>
</body>
   