<?php include 'header.php'; ?>
<section id="quemsomos" class="quemsomos">
    <div class="container">
        <h2>Quem Somos</h2>
        <div class="col-md-10 col-md-offset-1">
        <p>Nós somos uma startup de estabelecimentos de beleza, nosso objetivo é levar a você mais tempo, mais facilidade e mais praticidade, com o nosso serviço, você poderá ver as horas disponível no seu estabelecimento de beleza, seja um salão de beleza, uma barbearia ou até mesmo uma clínica de estética, chega de fila, chega de espera, agenda já online e gratuitamente, sem precisar estar ligando para confirma horário, é rapido e simples.</p>
    </div>
    </div>
</section>
<section id="convecimento" class="convecimento">
    <div class="container">
        <h2>Serviços</h2>
        <div class="col-md-4">
            <div class="perfil"><img src="assets/images/gestao.png" alt=""></div>
            <h4>Gestão Online</h4>
            <p>Tenha controle sobre o que você ganha, sobre o que você gasta, com a maior facilidade do mundo, saiba os dados do seu salão durante o período de contratação do serviço, hitórico e controle de clientes.</p>
        </div>
        <div class="col-md-4">
            <div class="perfil"><img class="especial" src="assets/images/calendario.png" alt=""></div>
            <h4>Agendamento Online</h4>
            <p>Possibilite o agendamento online para seu estabelecimento, oferecendo praticidade e comodidade sobre seu cliente, além de facilitar a vida de seu cliente.</p>
        </div>
        <div class="col-md-4">
            <div class="perfil"><img src="assets/images/marketing.png" alt=""></div>
            <h4>Marketing</h4>
            <p>Publique fotos no facebook e no instagram, sempre mostrando as novidades de seu estabelecimento, quando tiver um evento ou uma promoção imperdível para qualquer cliente, insira a foto em banner, que a foto do seu eveto ou promoção irá aparecer no nosso slide para todos verem.</p>
        </div>
    </div>
</section>
<style>
    .mocap{
        width: 100%; height: 385px;
    }    
</style>
<section id="explica" class="explica">
    <div class="container">
        <h2>Vantagens de se usar Minha Beleza Web</h2>
            <div class="col-md-3 col-md-offset-1">
                <img class="mocap" src="assets/images/imagens_m/mocap_iphone.png" alt="">
            </div>
        <div class="col-md-8">
        <p>Com Minha Beleza WEb, é muito prático e muito fácil de controla a gestão financeira, quando o seu cliente faz o agendamento pelo o nosso aplicativo, automaticamente, você sabe a hora que ele marca, e é enviado para o seu celular a soma de todos os serviços que ele agendou, trazendo no fim do dia, no fim da semana e no fim do mês, quanto você recebeu, sem precisar contar</p>
        <p>Com Minha beleza Web, é muito fácil de ter o controle e o histórico de seus clientes, além de um sistema ter um sistema de fidelidade, que o cliente, poderá ter pontos toda vez agendar pelo o nosso serviço, é sempre bom ter aquele cliente fiel, que só faz um serviço se for no seu estabelecimento</p>
        <p>Com Minha Beleza Web, é fácil de outras pessoas verem o seu salão, além de conhecer o histórico, as fotos, os profissionais, os serviços, a localização e as mídias sociais, conquistando cada vez mais o cliente através de seus trabalhos e de suas postagens na internet, fazendo então um marketing multinível, atraindo várias pessoas para seu salão, seja próximas ou distante, divulgue e atraia mais clientes.</p>
    </div>
    </div>
</section>
<section id="contrato" class="contrato">
    <div class="container">
        <h2>Compra e contrato</h2>
        <div class="col-md-4">
        <div class="compra">
            <h2>Mensal</h2>
            <h3>65 R$</h3>
            <ul>
                <li>Sem compromisso.</li>
                <li>Organização de clientes.</li>
                <li>Agendamento online para clientes.</li>
                <li>Controle Financeiro.</li>
                <li>Estatística de clientes mensal.</li>
                <li style="margin-bottom: -1px;">Estatística do estabelecimento mensal.</li>
            </ul>
            <a class="waves-effect waves-light btn">Compra</a>
        </div>
        </div>
        <div class="col-md-4">
        <div class="compra">    
            <h2>Semestral</h2>
            <h3>60 R$</h3>
            <ul>
                <li>Contrato semestral.</li>
                <li>Organização de clientes.</li>
                <li>Agendamento online para clientes.</li>
                <li>Controle Financeiro.</li>
                <li>Estatística de clientes semestrais e mensais.</li>
                <li style="margin-bottom: -1px;">Estatística do estabelecimento semestral e mensal.</li>
            </ul>
            <a class="waves-effect waves-light btn">Compra</a>
        </div>
        </div>
        <div class="col-md-4">
        <div class="compra">
            <h2>Anual</h2>
            <h3>55 R$</h3>
            <ul>
                <li>Contrato Anual.</li>
                <li>Organização de clientes.</li>
                <li>Agendamento online para clientes.</li>
                <li>Controle Financeiro.</li>
                <li>Estatística de clientes anual e mensal.</li>
                <li style="margin-bottom: -1px;">Estatística do estabelecimento anual e mensal.</li>
            </ul>
            <a class="waves-effect waves-light btn">Compra</a>
        </div>
        </div>
    </div>
</section>
<section id="testimonial" class="parallax-section">
        
			<div class="overlay"></div>
		<div class="container">	
				<div class="ts-testimonial-slide">
				
					<div class="ts-testimonial-item">
						<p>"Nossa que facilidade, nem preciso mais contar o dinheiro, agora tudo é automatizado, Minha Beleza Web é uma maravilha, eu recomendo bastante."</p>
						<div class="infor-client">
							<span class="icon-client"><img src="assets/images/icon-testimonial.png" alt=""></span>
							<span class="client-name">ROBERTA SAMURAI</span>
							<span class="client-position">Agende já em <a style="color: blue;" href="#">robertasamurai/meusalaoweb.com</a></span>
						</div>
					</div>
					<div class="ts-testimonial-item">
						<p>"Que legal, minha renda de clientes aumentou 60%, algo que eu nunca imaginei, é um sistema fantástico. Eu recomendo para os barbeiros."</p>
						<div class="infor-client">
							<span class="icon-client"><img src="assets/images/icon-testimonial.png" alt=""></span>
							<span class="client-name">MIGUEL SOAQUINO</span>
							<span class="client-position">Agende já em <a style="color: blue;" href="#">adersonsoaquino/meubarbeiroweb.com</a></span>
						</div>
					</div>
					<div class="ts-testimonial-item">
						<p>"Além de ganhar clientes, economizei meu tempo, autonomizando algo simples, mas que pela a facilidade, gera muitos resultados, eu recomendo bastante, para as clinicas de estética."</p>
						<div class="infor-client">
							<span class="icon-client"><img src="assets/images/icon-testimonial.png" alt=""></span>
							<span class="client-name">JORAIMA SAVARE</span>
							<span class="client-position">Agende já em <a style="color: blue;" href="#">joraimasavare/minhaesteticaweb.com</a></span>
						</div>
					</div>
				</div>
			</div>
    		</section>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
		<script>
      //TESTIMONIAL SLIDER
   if ($(".ts-testimonial-slide").length > 0) {

     $(".ts-testimonial-slide").owlCarousel({
       autoPlay: 6000,
       slideSpeed: 1000,
       navigation: false,
       pagination: true,
       singleItem: true
     });
   };
</script>
<section id="sociais" class="sociais">
    <div class="container">
        <h2>Redes Sociais</h2>
        <div class="col-md-5 col-md-offset-1">
        <h4>Instagram</h4>
<!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/0fb837d4660e56a6beeb90e3bceb2e25.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
        </div>
        <div class="col-md-5">
           <h4>Facebook</h4>
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmeusalaoweb%2F&tabs=messages&width=450&height=296&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="450" height="296" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
        </div>
    </div>
</section>
   <section id="cliente" class="cliente">
  <div class="container">

    <h2>Conheça nossos segmentos e Clientes.</h2>
    <div class="row">
      <div class="col-md-4 col-sm-6 col-lg-4">
        <a href="">
        <div class="photo-container">
          <div class="photo">
            <img src="assets/images/minha_estetica_web_cor.png" alt="Petunia the pug">
            <div class="photo-overlay">
              <h2>Minha Estética Web</h2>
              <p>Procure já as barbearias mais próximas de você e agende já o seu serviço.</p>
            </div>
          </div>
        </div>
        </a>
      </div>
      <div class="col-md-4 col-sm-6 col-lg-4">
       <a href="../salao/index.php">
        <div class="photo-container">
          <div class="photo">
            <img src="assets/images/meu_salao_web_cor.png" alt="Red Facade">
            <div class="photo-overlay">
              <h2>Meu Salão Web</h2>
              <p>Procure já os salões mais próximos de você e agende já o seu serviço.</p>
            </div>
          </div>
        </div>
          </a>
      </div>
      <div class="col-md-4 col-sm-3 col-lg-4">
       <a href="">
        <div class="photo-container">
          <div class="photo">
            <img src="assets/images/minha_barbearia_web_cor.png" alt="Ferris Wheel">
            <div class="photo-overlay">
              <h2>Meu Barbeiro Web</h2>
              <p>Procure já as clínicas de estética mais próximas de você e agende já o seu serviço.</p>
            </div>
          </div>
        </div>
        </a>
      </div>
    </div>
  </div>
</section>
<?php include 'footer.php'; ?>
