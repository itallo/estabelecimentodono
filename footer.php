<section class="footer">
  <div class="container">
   <div class="row">
    <div class="col-md-3 col-md-offset-1">
      <div class="contato">
        <h5>Contato :</h5>
        <p></p>
        <p>85 986409160</p>
      </div>
    </div>
    <div class="col-md-3">
      <div class="info">
        <div class="row">
          <div class="contato">
            <h5>E-mail :</h5>
            <p>meusalaoweb@gmail.com</p>
            <h5>Redes Sociais :</h5>
            <div class="icones">
              <i><img id="instagram" src="assets/images/icones/instagram-preto.png" onmouseout="trocaInsta()" onmousemove="trocaInsta2()"></i>
              <i><img id="facebook" src="assets/images/icones/facebook-preto.png" onmousemove="trocaFace2()" onmouseout="trocaFace()"></i>
              <i><img id="twitter" src="assets/images/icones/twitter-preto.png" onmouseout="trocaTwitter()" onmousemove="trocaTwitter2()"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3 col-md-offset-1"> 
      <div class="contato">
        <h5>Nossos Sites :</h5>
        <ul>
          <li><a href="">MinhaBelezaWeb.com</a></li>
          <li><a href="">MinhaEstéticaWeb.com</a></li>
          <li><a href="">MinhaBarbeariaWeb.com</a></li>
          <li><a href="">MinhaBelezaWeb/Cliente.com</a></li>
          <li><a href="">MinhaBelezaWeb/Proprietario.com</a></li>
        </ul>
      </div>
    </div>
    </div>
    <div class="row">
    <p>MeusalãoWeb.com-2016</p>
    </div>
  </div>
</section>


<script>
function trocaFace() {
  document.getElementById("facebook").src ="assets/images/icones/facebook-preto.png";
}
function trocaFace2() {
  document.getElementById("facebook").src ="assets/images/icones/facebook-branco.png";
}
function trocaTwitter() {
  document.getElementById("twitter").src ="assets/images/icones/twitter-preto.png";
}
function trocaTwitter2() {
  document.getElementById("twitter").src ="assets/images/icones/twitter-branco.png";
}
function trocaInsta() {
  document.getElementById("instagram").src ="assets/images/icones/instagram-preto.png";
}
function trocaInsta2() {
  document.getElementById("instagram").src ="assets/images/icones/instagram-branco.png";
}
</script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. --
<script>
var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<script type='text/javascript' src='build/js/jquery.min.js'></script>
<script type='text/javascript' src='build/js/scripts.min.js'></script>
<script type='text/javascript' src='build/js/main.min.js'></script>
<!--
<script type='text/javascript' src='jquery.fancybox.js'></script>
<!-- <script type='text/javascript' src='build/js/vendor/bootstrap.min.js'></script> --


<script type="text/javascript">
jQuery(document).ready(function($) {
$(".fancybox").fancybox({
openEffect	: 'none',
closeEffect	: 'none'
});
});
</script>
-->
<script>
new WOW().init();
</script>
<script type="text/javascript">
var options = [ {selector: '#staggered-test', offset: 50, callback: function(el) { Materialize.toast("This is our ScrollFire Demo!", 1500 ); } }, {selector: '#staggered-test', offset: 205, callback: function(el) { Materialize.toast("Please continue scrolling!", 1500 ); } }, {selector: '#staggered-test', offset: 400, callback: function(el) { Materialize.showStaggeredList($(el)); } }, {selector: '#image-test', offset: 500, callback: function(el) { Materialize.fadeInImage($(el)); } } ]; Materialize.scrollFire(options);

</script>
</body>
</html>
